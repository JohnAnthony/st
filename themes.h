static const char *solarized[] = {
	/* 8 normal colors */
	"#222222", // black
	"#9e5641", // red
	"#6c7e55", // green
	"#caaf2b", // yellow
	"#7fb8d8", // blue
	"#956d9d", // magenta
	"#4c8ea1", // "cyan" (orange)
	"#808080", // white

	/* 8 bright colors */
	"#454545", // black
	"#cc896d", // red
	"#c4df90", // green
	"#ffe080", // yellow
	"#b8ddea", // blue
	"#c18fcb", // magenta
	"#6bc1d0", // "cyan" (orange)
	"#cdcdcd", // white

	[255] = 0,

	/* more colors can be added after 255 to use with DefaultXX */
	"#cccccc",
	"#555555",
	"gray90", /* default foreground colour */
	"black", /* default background colour */
};

static const char *ja[] = {
	/* 8 normal colors */
	"#101010", // black
	"#bf1e2d", // red
	"#9dba3a", // green
	"#f6b915", // yellow
	"#1ca1db", // blue
	"#652f90", // magenta
	"#ea7d24", // "cyan" (orange)
	"#ffffff", // white

	/* 8 bright colors */
	"#666666", // black
	"#ff0000", // red
	"#a9c938", // green
	"#f7d325", // yellow
	"#00afda", // blue
	"#c18fcb", // magenta
	"#f79321", // "cyan" (orange)
	"#f1f1f1", // white

	[255] = 0,

	/* more colors can be added after 255 to use with DefaultXX */
	"#cccccc",
	"#555555",
	"gray90", /* default foreground colour */
	"black", /* default background colour */
};

static const char *molokai[] = {
	/* 8 normal colors */
	"#272822",
	"#d0d0d0",
	"#66aa11",
	"#c47f2c",
	"#30309b",
	"#7e40a5",
	"#3579a8",
	"#9999aa",

	/* 8 bright colors */
	"#303030",
	"#ff0090",
	"#80ff00",
	"#ffba68",
	"#5f5fee",
	"#bb88dd",
	"#4eb4fa",
	"#ffffff",

	[255] = 0,

	/* more colors can be added after 255 to use with DefaultXX */
	"#cccccc",
	"#555555",
	"gray90", /* default foreground colour */
	"black", /* default background colour */
};

static const char *tender[] = {
	/* 8 normal colors */
	"#101010",
	"#f43753",
	"#c9d05c",
	"#ffc24b",
	"#b3deef",
	"#d3b987",
	"#73cef4",
	"#eeeeee",
	/* 8 bright colors */
	"#1d1d1d",
	"#f43753",
	"#c9d05c",
	"#ffc24b",
	"#b3deef",
	"#d3b987",
	"#73cef4",
	"#ffffff",

	[255] = 0,

	/* more colors can be added after 255 to use with DefaultXX */
	"#cccccc",
	"#555555",
	"gray90", // Foreground
	"black", // Background
};
